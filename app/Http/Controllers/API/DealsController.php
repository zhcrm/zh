<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = '
            {
                "data": [
                   {
                        "Closing_Date": "2018-04-20",
                        "Deal_Name": "TED Corp Deal",
                        "Expected_Revenue": 50000,
                        "Stage": "Negotiation/Review",
                        "Amount": 50000,
                        "Probability": 75
                    }
                ]
            }
        ';

        $client = new \GuzzleHttp\Client(['base_uri' => 'https://www.zohoapis.com/crm/v2/']);

        $res =$client->request('POST', 'deals', [
            'headers' => [
                'Authorization' => 'Bearer 1000.7de63b4550cf88b3124093ba8938919e.bf41534a3129cdc0ea50de21ff81d6e8',
                //'Content-type' => 'application/json',
                //'Accept'     => 'application/json'
            ],
            'body' => $data,
        ]);

        echo $res->getBody();

        //return json_decode($res->getBody(), true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
