<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://accounts.zoho.com/oauth/v2/']);

        $res = $client->request('POST', 'token', [
            'form_params' => [
                'code' => '1000.3e20dc54f1e2bb7532eb15825a01471d.17b44364e1fb3d255e178741c276b6db',
                'redirect_uri' => 'http://www.zh.com/oauth2callback',
                'client_id' => '1000.E66ZGEIH2UCQ86252TOF81WZJ7P6NA',
                'client_secret' => '9fb9fbef43a4fb96706cb5c8e004cea19830e60960',
                'grant_type' => 'authorization_code',
            ]
        ]);

        echo $res->getBody();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
