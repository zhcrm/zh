<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = '
            {
                "data": [
                   {
                        "Subject": "Call",
                        "Due_Date ": "2018-04-12",
                        "Status": "Deferred",
                        "$se_module": "Deals",
                        "What_Id": "3615890000000216013"
                    }
                ]
            }
        ';

        $client = new \GuzzleHttp\Client(['base_uri' => 'https://www.zohoapis.com/crm/v2/']);

        $res =$client->request('POST', 'tasks', [
            'headers' => [
                'Authorization' => 'Bearer 1000.7de63b4550cf88b3124093ba8938919e.bf41534a3129cdc0ea50de21ff81d6e8',
                //'Content-type' => 'application/json',
                //'Accept'     => 'application/json'
            ],
            'body' => $data,
        ]);

        echo $res->getBody();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
